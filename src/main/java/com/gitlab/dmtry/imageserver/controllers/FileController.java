package com.gitlab.dmtry.imageserver.controllers;

import com.gitlab.dmtry.imageserver.service.FileService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log4j2
@RestController
@AllArgsConstructor
public class FileController {

    private final FileService fileService;

    @GetMapping(value = "/image/{file_name}")
    public @ResponseBody
    ResponseEntity<byte[]> getImage(@PathVariable(value = "file_name") String fileName)
            throws IOException {
        var headers = new HttpHeaders();

        var fileDto = fileService.getFile(fileName);
        headers.setContentType(fileDto.getMediaType());
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        return new ResponseEntity<>(fileDto.getFile(), headers, HttpStatus.OK);
    }


    @PostMapping(value = "/image")
    public ResponseEntity<String> uploadImage(@RequestParam("file") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        String savedFileName = fileService.saveFile(file);
        return new ResponseEntity<>(savedFileName, HttpStatus.CREATED);
    }
}
