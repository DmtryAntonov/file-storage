package com.gitlab.dmtry.imageserver.dto;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.MediaType;

@Builder
@Getter
public class FileDto {

    private MediaType mediaType;

    private byte[] file;
}
