package com.gitlab.dmtry.imageserver.exceptions;

public class WrongFileTypeException extends RuntimeException{
    public WrongFileTypeException(String message) {
        super(message);
    }
}
