package com.gitlab.dmtry.imageserver.exceptions;

public class WrongPathException extends RuntimeException{

    public WrongPathException(Throwable cause) {
        super(cause);
    }
}
