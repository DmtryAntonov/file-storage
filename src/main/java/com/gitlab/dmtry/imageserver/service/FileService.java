package com.gitlab.dmtry.imageserver.service;

import com.gitlab.dmtry.imageserver.controllers.FileController;
import com.gitlab.dmtry.imageserver.dto.FileDto;
import com.gitlab.dmtry.imageserver.exceptions.WrongFileTypeException;
import com.gitlab.dmtry.imageserver.exceptions.WrongPathException;
import com.gitlab.dmtry.imageserver.util.StringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Log4j2
@Service
public class FileService {

    private Path sharedFolder;

    private final String fileFolder;

    private final String allowedFilenameChars;

    private final Long generatedNameLength;

    public FileService(@Value("${files.folder}") String fileFolder,
                       @Value("${files.filename-chars}") String allowedFilenameChars,
                       @Value("${files.generated-name-length}") Long generatedNameLength) {
        this.fileFolder = fileFolder;
        this.allowedFilenameChars = allowedFilenameChars;
        this.generatedNameLength = generatedNameLength;
    }

    @PostConstruct
    public void init() {
        //найдем путь где будут хранится файлы
        var pathInString = FileController.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        try {
            var decodedPath = URLDecoder.decode(pathInString, StandardCharsets.UTF_8);
            var file = new File(decodedPath);
            String rootFolder = file.getParentFile()
                    .getParentFile()
                    .getParent()
                    .replaceFirst("^(file:\\\\?)", "");

            sharedFolder = Paths.get(rootFolder, fileFolder);
            if (!Files.exists(sharedFolder)) {
                Files.createDirectory(sharedFolder);
            }
        } catch (IOException e) {
            log.error("Ошибка создания необходимой струкруры папкок", e);
            throw new WrongPathException(e);
        }
    }

    private Path buildFilePath(String fileName) {
        return Paths.get(sharedFolder.toString(),
                fileName);
    }

    public boolean fileIsExist(String fileName) {
        return Files.exists(buildFilePath(fileName));
    }

    public byte[] byteArrayFromFile(String fileName) throws IOException {
        return IOUtils.toByteArray(new FileInputStream(buildFilePath(fileName).toString()));
    }

    public FileDto getFile(String filename) throws IOException {
        if (!fileIsExist(filename)) {
            log.warn("File with name " + filename + " does not found");
            throw new FileNotFoundException(filename);
        }

        var fileMimeType = getFileMimeType(filename);

        return FileDto.builder()
                .file(byteArrayFromFile(filename))
                .mediaType(MediaType.parseMediaType(fileMimeType))
                .build();
    }

    private String getFileMimeType(String filename) throws IOException {
        var fullPath = buildFilePath(filename);
        return Files.probeContentType(fullPath);
    }

    @SuppressWarnings("javasecurity:S2083")
    public String saveFile(MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        var contentType = file.getContentType();
        if (!isValidMediaType(contentType)) {
            throw new WrongFileTypeException("wrong media type");
        }
        var extension = FilenameUtils.getExtension(file.getOriginalFilename());
        var filename = generateName(extension);
        Files.write(Paths.get(sharedFolder.toString(), filename), bytes);
        return filename;
    }

    private String generateName(String extension) {
        var filename = StringUtil.generateRandomString(generatedNameLength, allowedFilenameChars);
        return String.format("%s.%s", filename, extension);
    }

    public boolean isValidMediaType(String contentType) {
        MediaType mediaType;
        try {
            mediaType = MediaType.parseMediaType(contentType);
        } catch (InvalidMediaTypeException e) {
            return false;
        }
        return mediaType.equals(MediaType.IMAGE_JPEG) || mediaType.equals(MediaType.IMAGE_PNG);
    }
}
