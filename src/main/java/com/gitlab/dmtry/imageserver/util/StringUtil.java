package com.gitlab.dmtry.imageserver.util;

import lombok.experimental.UtilityClass;

import java.security.SecureRandom;
import java.util.stream.Collectors;

@UtilityClass
public class StringUtil {

    /**
     * Генерирует случайную строку
     *
     * @param generatedNameLength длина случайной строки
     * @param allowedFilenameChars разрешенные символы
     * @return случайная строка
     */
    public static String generateRandomString(Long generatedNameLength, String allowedFilenameChars) {
        return new SecureRandom()
                .ints(generatedNameLength, 0, allowedFilenameChars.length())
                .mapToObj(allowedFilenameChars::charAt)
                .map(Object::toString)
                .collect(Collectors.joining());
    }
}
