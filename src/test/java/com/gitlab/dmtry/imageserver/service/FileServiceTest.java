package com.gitlab.dmtry.imageserver.service;

import com.gitlab.dmtry.imageserver.exceptions.WrongFileTypeException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

class FileServiceTest {

    private FileService fileService;

    private static final String STORAGE_FOLDER = "folder";
    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final String FULL_STORAGE_PATH = "build/" + STORAGE_FOLDER + "/";
    private static final String ALLOWED_FILENAME_CHARS = "abc";
    private static final Long GENERATED_NAME_LENGTH = 3L;

    @BeforeEach
    void before() {
        fileService = new FileService(STORAGE_FOLDER,
                ALLOWED_FILENAME_CHARS,
                GENERATED_NAME_LENGTH);
        fileService.init();
    }

    @Test
    void isValidMediaTypePngTest() {
        Assertions.assertTrue(fileService.isValidMediaType("image/png"));
    }

    @Test
    void isValidTypeJpegTest() {
        Assertions.assertTrue(fileService.isValidMediaType("image/jpeg"));
    }

    @Test
    void isValidTypeNullTest() {
        Assertions.assertFalse(fileService.isValidMediaType(null));
    }

    @Test
    void isValidTypeEmptyTest() {
        Assertions.assertFalse(fileService.isValidMediaType(""));
    }

    @Test
    void isValidTypeBmpTest() {
        Assertions.assertFalse(fileService.isValidMediaType("image/bmp"));
    }

    @Test
    void fileIsExistTest() throws IOException{
        var fileName = "alreadyexist.jpeg";
        var filePath = FULL_STORAGE_PATH + fileName;
        createFile(filePath);
        Assertions.assertTrue(fileService.fileIsExist(fileName), "Файл не сущесвует");
    }

    @Test
    void fileIsNotExistTest() {
        var fileName = "qwerty.jpeg";
        Assertions.assertFalse(fileService.fileIsExist(fileName), "Файл существует");
    }

    @Test
    void byteArrayFromFileTest() throws IOException{
        var filename = "testImage.jpg";
        copyTestFileToStorageFolder(filename);
        var bytes = fileService.byteArrayFromFile(filename);
        Assertions.assertTrue(bytes.length > 0, "Файл не вернулся");
    }

    @Test
    void saveFileTest() throws IOException {
        var savedFileName = fileService.saveFile(createFile("qwerty.jpeg", "image/jpeg"));
        Assertions.assertFalse(StringUtils.isEmpty(savedFileName), "Имя нового файла не вернулось");
        var savedFile = new File(FULL_STORAGE_PATH + savedFileName);
        Assertions.assertTrue(savedFile.exists(), "Файл не сохранен");
    }

    @Test
    void saveFileWrongFileTypeTest() {
        var file = createFile("qwerty.bmp", "image/bmp");
        Assertions.assertThrows(WrongFileTypeException.class,
                () -> fileService.saveFile(file));
    }

    @Test
    void getFileTest() throws IOException {
        var filename = "testImage.jpg";
        copyTestFileToStorageFolder(filename);
        var fileDto = fileService.getFile(filename);

        Assertions.assertFalse(Objects.isNull(fileDto.getFile()), "Файл не вернулся");
        Assertions.assertFalse(Objects.isNull(fileDto.getMediaType()), "Media Type не вернулся");
        Assertions.assertEquals(fileDto.getMediaType(), MediaType.IMAGE_JPEG, "Media type определен не правильно");
    }

    @Test
    void getFileNotFoundTest() throws IOException {
        var filename = "random.jpeg";
        Assertions.assertThrows(FileNotFoundException.class,
                () -> fileService.getFile(filename));
    }

    private void createFile(String filePath) throws IOException {
        var file = new File(filePath);
        file.createNewFile();
    }

    private void copyTestFileToStorageFolder(String filename) throws IOException {
        var fromFile = new File(RESOURCES_PATH + "/" + filename);
        var to = Paths.get(FULL_STORAGE_PATH + filename);
        var from = fromFile.toPath();
        Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
    }

    private MultipartFile createFile(String fileName, String contentType) {
      return new MultipartFile() {
          @Override
          public String getName() {
              return fileName;
          }

          @Override
          public String getOriginalFilename() {
              return fileName;
          }

          @Override
          public String getContentType() {
              return contentType;
          }

          @Override
          public boolean isEmpty() {
              return false;
          }

          @Override
          public long getSize() {
              return 0;
          }

          @Override
          public byte[] getBytes() throws IOException {
              return new byte[0];
          }

          @Override
          public InputStream getInputStream() throws IOException {
              return null;
          }

          @Override
          public void transferTo(File dest) throws IOException, IllegalStateException {
          }
      };
    };
}