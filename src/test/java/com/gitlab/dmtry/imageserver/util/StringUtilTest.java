package com.gitlab.dmtry.imageserver.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilTest {

    private final static String ALLOWED_CHARS = "ABCabc";
    private final static Long STRING_LENGTH = 12L;

    @Test
    void generateRandomString() {
        String generateRandomString = StringUtil.generateRandomString(STRING_LENGTH, ALLOWED_CHARS);
        Assertions.assertEquals(STRING_LENGTH, generateRandomString.length(), "Длина строки не равна заданной");
    }
}